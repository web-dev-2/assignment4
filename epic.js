"strict"

!(function(){


document.addEventListener("DOMContentLoaded", function (e)
{

  //hi sir sorry for the poor variable choises.
  // i changed them multiple times beause the original variable names were even worst
  let infoList=[];
  let imageMenue=document.getElementById('image-menu');
  let template=document.getElementById('image-menu-item');
  let eid=document.getElementById('earth-image-date');
  let eit=document.getElementById('earth-image-title');
  let img=document.getElementById('earth-image');
  let dateInput=document.getElementById("date");
  let imgTyp=document.getElementById("type");
  let date=document.getElementById("date");
      imgTyp.addEventListener('change', function() {
        mostRecentDate(imgTyp.value);
      });
  
  

  const form=document.getElementById('request_form');
  form.addEventListener("submit",function (e){
    e.preventDefault();

    fetch(`https://epic.gsfc.nasa.gov/api/${imgTyp.value}/date/${date.value}`)
      .then(response => { 
        if (!response.ok) {
          throw new Error("Not 2xx response",{cause: response});

        }
        return response.json(); 
      })

      .then(obj => {
        console.log("2) Deserialized response:", obj)
        //i tried to display a message when there 
        if(obj.length===0)
        {
        //   imageMenue.textContent='';
        //   const parent=document.importNode(template.content,true);
        //   let child=parent.querySelector('span');
        //   child.parentNode.removeChild(child);

        //   parent.children[0].textContent="No Data Available";
        //   imageMenue.appendChild(parent);

        img.src="undefinded";
        eid.textContent=" ";
        eit.textContent=" ";

        }

        infoList=obj;
        displayDates(infoList,date.value);
        mostRecentDate(imgTyp.value);
      })
      .catch(error => { 
        console.log("3) Error:", error) 
      });
  })

  function displayDates(infoList,originalDate)
  {
    imageMenue.textContent=undefined;
    infoList.forEach((oneDate, i)=>{
    const dateData=document.importNode(template.content,true);
    dateData.querySelector('li').setAttribute('data-image-list-index',i);
    dateData.querySelector('span').textContent=oneDate.date;
    dateData.querySelector('li').addEventListener('click', function(){
      displayImg(originalDate,oneDate);

      eid.textContent=oneDate.date;
      eit.textContent=oneDate.caption

    });

    imageMenue.appendChild(dateData);

    })
  }

  function displayImg(originalDate,oneDate)
  {
    let type=document.getElementById('type').value;
    let arr=originalDate.split('-');
    

    img.src=`https://epic.gsfc.nasa.gov/archive/${type}/${arr[0]}/${arr[1]}/${arr[2]}/jpg/${oneDate.image}.jpg`

  }

    function mostRecentDate(imgTyp)
    {
      fetch(`https://epic.gsfc.nasa.gov/api/${imgTyp}/all`)
      .then(response => { 
        
        if (!response.ok) {
          throw new Error("Not 2xx response",{cause: response});

        }
        return response.json(); 
      })

      .then(obj => { 
        console.log("2) Deserialized response:", obj)
        let all=[];
        all=obj;
        all.sort((a,b) => (b-a));
        dateInput.setAttribute('max', all[0].date);
        dateInput.setAttribute('min', all[all.length-1].date);

      })
      .catch(error => { 
        console.log("3) Error:", error) 
      });

    }

    
   


})

}());